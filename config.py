from dotenv import load_dotenv
import os

load_dotenv()

dbUser = os.getenv("DB_USER")
dbPass = os.getenv("DB_PASS")
dbHost = os.getenv("DB_HOST")
dbPort = os.getenv("DB_PORT")

