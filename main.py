from curses import keyname
from email.mime import application
import json
from textwrap import indent
from gridfs import Database
import requests
import os
import pymongo
from dotenv import load_dotenv
from datetime import datetime
from database import getDatabase

load_dotenv()

CF_TOKEN = os.getenv("CF_TOKEN")
BASE_URL = "https://api.cloudflare.com/client/v4/zones/"
CF_ZONE_ID = os.getenv("CF_ZONE_ID")
CF_EMAIL = os.getenv("CF_EMAIL")



CF_HEADER = {
    'Authorization' : 'Bearer ' + CF_TOKEN,
    'Content-Type' : 'application/json'
}
CF_PATH = BASE_URL + CF_ZONE_ID + "/dns_records/"


listDNS = requests.get(url=CF_PATH, headers=CF_HEADER)

date = datetime.now().strftime("%Y_%m_%d-%I_%m_%S_%p")
fileName = ("domainResult-" + date + ".json")
listDNSDict=listDNS.json()

domainResult = dict()
dbName = getDatabase()
collectionName = dbName['domain_api_results']

for result in listDNSDict['result']:
    with open("API-Result.json", "a") as outputAPI:
        outputAPI.write(json.dumps(result))
        outputAPI.write("\n")
        collectionName.insert_one(result)
        

        for (key,value) in result.items():
            if key == "id" or key == 'name' or key == 'type' or key == 'content' or key == 'created_on' or key == "modified_on":
             domainResult[key] = value
      
       # print(domainResult)
        with open(fileName, "a") as outputFile:
            outputFile.write(json.dumps(domainResult))
            outputFile.write("\n")

colletionList = collectionName.find()

for item in colletionList:
    print (item)    
            


if result == item:
        print("Nothing New")
else:
        print("Something New")